function [ DOA_est, Spec_est] = COMET_2D( Y, NSrc, Grid, AGrid)
%COMET_2D returns the estimated DOAs and the corresponding 2D matrix
%containing the value of the COMET function in the case of two source
%signals. In this function, simple nested for-loops are used to search for
%the global minimum
%
%Input:   Y: Received signal (NAnt x NSnp)
%         NSrc: Number of sources
%         Grid: the sampled angle of view (1 x NGrid)
%         AGrid: the dictionary steering matrix corresponding to the
%         sampled angle of view (NAnt x NGrid)
%         Ryy: the covariance matrix of the received signal (optional)
%
% Output: DOA_est: the estimated DOA on the sampled angle of view (1 x NSrc)
%         Spec_est: the objective function for each value of the gridpoints


% Initialize parameters
[NAnt, NSnp] = size(Y);
R_est = 1/NSnp * (Y*Y');
NGrid = length(Grid);

diagInd = 1:NAnt;
realInd = (diagInd(end)+1): (diagInd(end) + NAnt*(NAnt-1)/2);
imagInd = (realInd(end)+1): (NAnt^2);

boolFlagMat = tril(true(NAnt, NAnt), -1);
boolFlag = boolFlagMat(:);

AGridTilde = (sqrtm(R_est))\AGrid;
r_est = zeros(NAnt^2, 1);
r_est(diagInd) = 1;

% Prepare and calculate the 2D spectrum
N2DGrid = NGrid*(NGrid-1)/2;
Spec_vec = zeros(N2DGrid, 1);
Spec_est = zeros(NGrid, NGrid);
lin2DInd = 0;
A_tilde = zeros(NAnt^2, 5);
invR = inv(R_est);
tempAGrid = reshape(AGridTilde, [NAnt 1 NGrid]).*reshape(conj(AGridTilde), [1 NAnt NGrid]);

% A_tilde(:, 5) = extractRealParam(inv(R_est), boolFlag);
A_tilde(diagInd, 5) = real(diag(invR));
A_tilde(realInd, 5) = real(invR(boolFlag));
A_tilde(imagInd, 5) = imag(invR(boolFlag));
for (iGrid = 1:NGrid-1)
    tempMat1 = tempAGrid(:, :, iGrid);
    A_tilde(diagInd, 1) = real(diag(tempMat1));
    A_tilde(realInd, 1) = real(tempMat1(boolFlag));
    A_tilde(imagInd, 1) = imag(tempMat1(boolFlag));
    for (jGrid = iGrid+1:NGrid)
        lin2DInd = lin2DInd + 1;
        tempMat2 = tempAGrid(:, :, jGrid);
        A_tilde(diagInd, 2) = real(diag(tempMat2));
        A_tilde(realInd, 2) = real(tempMat2(boolFlag));
        A_tilde(imagInd, 2) = imag(tempMat2(boolFlag));
        
        tempMat3 = 2*AGridTilde(:, jGrid)*AGridTilde(:, iGrid)';
        A_tilde(diagInd, 3) = diag(real(tempMat3));
        A_tilde(realInd, 3) = real(tempMat3(boolFlag));
        
        A_tilde(diagInd, 4) = diag(-1*imag(tempMat3));
        A_tilde(realInd, 4) = -1*imag(tempMat3(boolFlag));
        Spec_vec(lin2DInd) = r_est'*A_tilde*(A_tilde\r_est);
    end
end
[~, pos2DInd] = max(Spec_vec);
indGrid = reshape(tril(true(NGrid, NGrid), -1), [], 1); 
Spec_est(indGrid) = 1:N2DGrid;
linInd = find(Spec_est==pos2DInd);
[iOpt, jOpt] = ind2sub(NGrid, linInd);
DOA_est = [Grid(jOpt); Grid(iOpt)];
Spec_est(indGrid) = Spec_vec;
Spec_est = Spec_est + Spec_est';
end
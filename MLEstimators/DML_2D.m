function [ DOA_est, Spec_est] = DML_2D( Y, NSrc, Grid, AGrid, varargin )
%DML_2D returns the estimated DOAs and the corresponding 2D matrix
%containing the value of the DML function in the case of two source
%signals. In this function, simple nested for-loops are used to search for
%the global minimum. The cost function of DML for two source signals are
%rewritten for faster computation
%
%Input:   Y: Received signal (NAnt x NSnp)
%         NSrc: Number of sources
%         Grid: the sampled angle of view (1 x NGrid)
%         AGrid: the dictionary steering matrix corresponding to the
%         sampled angle of view (NAnt x NGrid)
%         Ryy: the covariance matrix of the received signal (optional)
%
% Output: DOA_est: the estimated DOA on the sampled angle of view (1 x NSrc)
%         Spec_est: the objective function for each value of the gridpoints


% Initialize parameters
[NAnt, NSnp] = size(Y);
NGrid = length(Grid);
if isempty(varargin)
    R_est = 1/NSnp*(Y*Y');
else
    R_est = varargin{1};
end
% tic
AHA = AGrid'*AGrid;
AHRA = AGrid'*R_est*AGrid;
% Prepare and calculate the 2D spectrum
Spec_est = zeros(NGrid, NGrid);
for (iGrid = 1:NGrid-1)
    for (jGrid = iGrid+1:NGrid-1)
        a1Ha2 = AHA(iGrid, jGrid);
        a1HRa1 = real(AHRA(iGrid, iGrid));
        a2HRa2 = real(AHRA(jGrid, jGrid));
        a2HRa1 = AHRA(jGrid, iGrid);
        Spec_est(iGrid, jGrid) = 1/(NAnt^2 - abs(a1Ha2)^2)*(NAnt*(a1HRa1 + a2HRa2)-2*real(a1Ha2*a2HRa1));
    end
end
% toc
% figure
% imagesc(Spec_est)
% Search for the global maximum and estimated DOAs
[~, linInd] = min(1./Spec_est(:));
[iOpt, jOpt] = ind2sub(NGrid, linInd);
DOA_est = [Grid(iOpt); Grid(jOpt)];

end
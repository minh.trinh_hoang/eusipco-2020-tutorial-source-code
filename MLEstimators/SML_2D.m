function [ DOA_est, Spec_est] = SML_2D( Y, NSrc, Grid, AGrid, varargin )
%SML_2D returns the estimated DOAs and the corresponding 2D matrix
%containing the value of the SML function in the case of two source
%signals. In this function, simple nested for-loops are used to search for
%the global minimum
%
%Input:   Y: Received signal (NAnt x NSnp)
%         NSrc: Number of sources
%         Grid: the sampled angle of view (1 x NGrid)
%         AGrid: the dictionary steering matrix corresponding to the
%         sampled angle of view (NAnt x NGrid)
%         Ryy: the covariance matrix of the received signal (optional)
%
% Output: DOA_est: the estimated DOA on the sampled angle of view (1 x NSrc)
%         Spec_est: the objective function for each value of the gridpoints


% Initialize parameters
[NAnt, NSnp] = size(Y);
NGrid = length(Grid);
if isempty(varargin)
    R_est = 1/NSnp*(Y*Y');
else
    R_est = varargin{1};
end
% tic
Spec_est = zeros(NGrid, NGrid);
for (iGrid = 1:NGrid-1)
    for (jGrid = iGrid+1:NGrid-1)
        A = [AGrid(:, iGrid), AGrid(:, jGrid)];
        pinvA = A\eye(NAnt);
        % this is equivalent to
        % det(A*inv(A'*A)*A'*R_est*A*inv(A'*A)*A' + trace((eye(NAnt)-A*inv(A'*A)*A')*R_est)/(NAnt-NSrc)*(eye(NAnt)-A*inv(A'*A)*A') )
        Spec_est(iGrid, jGrid) = 1/real(det(A*pinvA*R_est*A*pinvA + trace((eye(NAnt)-A*pinvA)*R_est)/(NAnt-NSrc)*(eye(NAnt)-A*pinvA)));
    end
end
% toc
% figure
% imagesc(Spec_est)
% Search for the global minimum and estimated DOAs
[~, linInd] = max(Spec_est(:));
[iOpt, jOpt] = ind2sub(NGrid, linInd);
DOA_est = [Grid(iOpt); Grid(jOpt)];

end
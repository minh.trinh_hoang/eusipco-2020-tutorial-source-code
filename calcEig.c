/* This function tracks the eigenvalues of a Hermitian matrix perturbed by
 * a Hermitian rank-one matrix D + \rho*z*z^H.
 * Input:
 *        d: a vector of size 1 x NAnt: the initial eigenvalues without
 *           any perturbation, which is also the elements on the diagonal of
 *           the matrix D. Elements of d must be sorted ascendingly
 *        rho: a row vector of size 1 x NGrid: the scaling factor. Must be real
 *        absz2: a row vector of size 1 x (NAnt*NGrid): contains the absolute
 *               value squared of the vector z for each grid point. For example,
 *               the first NAnt-values of absz2 are the absolute value squared
 *               of the first gridpoint and so on...
 *        eigOrder: a row vector of size 1 x NEig, contains the order of the
 *                  eigenvalues that should be tracked. The values in eigOrder
 *                  must be between 1 and NAnt.
 *        nAnt: the size of the matrix D
 *        nGrid: the number of gridpoints (steering vectors) for tracking
 * Output:
 *        eigVal: a row vector of size 1 x (NEig*NsGrid): returns the tracked
 *                eigenvalues for each grid point. For example, the first nEig
 *                values of eigVal are the eigenvalues of the first grid point
 */

#include "mex.h"
#include "math.h"
#include <mkl_lapack.h>
#include <mkl_cblas.h>
#include <mkl.h>
#include <mkl_types.h>
#include <mkl_service.h>
#include <omp.h>

void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    /* input and output variables*/
    double *d;
    double *rho;
    double *absz;
    double *eigOrder;
	MKL_INT nAnt, index, info, nEig, nGrid;
    double *eigVal;              /* output matrix */
    
    /*temporary variables in the routine*/
    MKL_INT iGrid;
	double eig;
    
    /* Initialize values for input and output variables*/
    d = mxGetPr(prhs[0]);
    rho = mxGetPr(prhs[1]);
    absz = mxGetPr(prhs[2]);
    eigOrder = mxGetPr(prhs[3]);
    nEig = (MKL_INT)mxGetN(prhs[3]);
    nAnt = (MKL_INT)mxGetScalar(prhs[4]);
    nGrid = (MKL_INT)mxGetScalar(prhs[5]);
    plhs[0] = mxCreateDoubleMatrix(nEig,nGrid,mxREAL);
    eigVal = mxGetPr(plhs[0]);
    
	MKL_INT *intEigOrder = (MKL_INT*) mkl_malloc(nEig * sizeof(MKL_INT), 64);
	for (MKL_INT iEig = 0; iEig < nEig; iEig++) {
		intEigOrder[iEig] = (MKL_INT)eigOrder[iEig];
	}
    
    /* Main computation routine */
    double *delta = (double*) mkl_malloc(nAnt * sizeof(double), 64);
	for (iGrid = 0; iGrid < nGrid; iGrid++) {
        for (MKL_INT iEig = 0; iEig < nEig; iEig++) {
            dlaed4(&nAnt, intEigOrder + iEig, d, absz + iGrid*nAnt, delta, rho + iGrid, eigVal + (iGrid*nEig+iEig), &info);
        }
        
    }
    
    /* Free memory of temporary variables */
    mkl_free(intEigOrder);
	mkl_free(delta);
}

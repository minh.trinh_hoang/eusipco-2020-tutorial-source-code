/* This function calculates the pseudo-spectrum of the PR-UCF estimator. The idea is
 * to perform Newton's method on the derivative of the objective function with respect
 * to \rho (or in the paper \sigma_s^2). The method to determine the eigenvalues is based 
 * on the paper "Solving Secular Function Stably and Efficiently" using the
 * Middle Way method.
 * Input:
 *        d: a row vector of size 1 x NAnt: the initial eigenvalues without
 *           any perturbation, which is also the elements on the diagonal of
 *           the matrix D. Elements of d must be sorted ascendingly
 *        rho: a row vector of size 1 x NGrid: the scaling factor. Must be real
 *        absz: a row vector of size 1 x (NAnt*NGrid): contains the absolute
 *              value of each entry in the vector z for each grid point. For example,
 *              the first NAnt-values of absz are the absolute value of the first gridpoint
 *              and so on...
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *        NAnt: the size of the matrix D
 *        NGrid: the number of gridpoints (steering vectors) for tracking
 *		  bf_spec: a row vector of size 1 x NGrid: the beamformer spectrum
 *Output:
 *			numIt: a row vector of size 1 x NGrid: returns the number of bisection steps
 *				   for each gridpoint
 *			optimal_rho: a row vector of size 1 x NGrid: returns the optimal value of
 *						 rho (or in the paper \sigma_s^2) for each grid point
 *          objVal: a row vector of size 1 x NGrid: returns the optimal value of
 *					the PR-UCF optimization problem for each grid point
 */

#include "mex.h"
#include "matrix.h"
#include "math.h"
#include <mkl_lapack.h>
#include <mkl_cblas.h>
#include <mkl.h>
#include <mkl_types.h>
#include <mkl_service.h>
#include <omp.h>


/* This subfunction calculates the derivative of the objective function with respect to the scalar \rho (or equivalently \sigma_s^2). For more
 * information, please refer to Equation (65) in the paper
 * Input:
 *        bf_spec_perGrid: the conventional beamformer spectrum at the given gridpoint
 *        rho: the scalar at which the derivative is calculated
 *        eigVal: a row vector of size 1 x (NSrc-1): the eigenvalues of the modified Hermitian matrix
 *				  with the scalar \rho
 *		  absz_perGrid: a row vector of size 1 x NAnt, containing the absolute valued of each entry in vector z in the paper
						 See Equation (65)
 *        NAnt: the size of the matrix D
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *Output:
 *			derivVal: the first entry contains the derivative of the objective function at the point rho
*/
void calcDeriv(double bf_spec_perGrid, double k, double *eigVal, double *absz_perGrid, double *delta_mat, MKL_INT NAnt, MKL_INT NSrc, double *derivVal) {
	MKL_INT iSrc, iAnt;
	double derivSec1_perAnt, derivSec2_perAnt;
	double derivSec1, derivSec2;
	derivVal[0] = 0;
	derivVal[1] = 0;
	for (iSrc = 0; iSrc < (NSrc - 1); iSrc++) {
		derivSec1 = 0;
		derivSec2 = 0;
		for (iAnt = 0; iAnt < NAnt; iAnt++) {
			derivSec1_perAnt = absz_perGrid[iAnt]*absz_perGrid[iAnt] / delta_mat[iSrc*NAnt + iAnt] / delta_mat[iSrc*NAnt + iAnt];
			derivSec2_perAnt = -derivSec1_perAnt / delta_mat[iSrc*NAnt + iAnt];
			derivSec1 = derivSec1 + derivSec1_perAnt;
			derivSec2 = derivSec2 + derivSec2_perAnt;
		}
		derivVal[0] = derivVal[0] - 2 * eigVal[iSrc] / (k*k*derivSec1);
		derivVal[1] = derivVal[1] + (-4 * k*eigVal[iSrc] * derivSec1*derivSec1 + 4 * eigVal[iSrc] * derivSec2 + 2 * derivSec1) / (k*k*k*k*derivSec1*derivSec1*derivSec1);
	}
	derivVal[0] = derivVal[0] - 2 * bf_spec_perGrid/NAnt + 2 * k;
	derivVal[1] = 2 - derivVal[1];
}

/*This subfunction returns the eigenvalues of the modified Hermitian matrix.
* Basically it is a wrap function for the main dlaed4() routine of LAPACK.
* Input:
*        d: a row vector of size 1 x NAnt: the initial eigenvalues without
*           any perturbation, which is also the elements on the diagonal of
*           the matrix D. The elements of d must be sorted ascendingly
*        rho: the scaling factor of the rank-one term. Must be scalar and positive
*        absz_perGrid: a row vector of size 1 x NAnt: contains the absolute
*               value of the vector z for one grid point.
*        NAnt: the size of the matrix D
*        NSrc: the number of source signals (all the eigenvalues from the first to the (NSrc-1) will be calculated)
*
*Output:
*        eig: a row vector of size 1 x (NSrc-1): returns the calculated
*                eigenvalues for each grid point. The eigenvalues are sorted
*				 in ascending order.
*       delta_mat: a row vector containing the difference between the modified
*                  eigenvalues and the poles in the rational function
 *      info: output message of the dlaed4 routine
*/
void calcEig(double *d, double rho, double *absz_perGrid, MKL_INT NAnt, MKL_INT NSrc, double *eig, double* delta_mat, MKL_INT info) {
	for (MKL_INT iEig = 1; iEig <= NSrc - 1; iEig++) {
		dlaed4(&NAnt, &iEig, d, absz_perGrid, delta_mat + ((iEig - 1)*NAnt), &rho, eig + (iEig - 1), &info);
	}
}


void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    /* input variables*/
	double *d;
	double *absz;
	double *rho_init;
	MKL_INT NAnt;
	MKL_INT NSrc;
	MKL_INT NGrid;
	double *bf_spec;
	/* output matrix */
	double *numIt;
	double *optimal_rho;
	double *objVal;

	/* Variables */
	MKL_INT iGrid;


	/* Initialize values for input and output variables*/
	d = mxGetPr(prhs[0]);
	absz = mxGetPr(prhs[1]);
	rho_init = mxGetPr(prhs[2]);
	NSrc = (MKL_INT)mxGetScalar(prhs[3]);
	NAnt = (MKL_INT)mxGetScalar(prhs[4]);
	NGrid = (MKL_INT)mxGetScalar(prhs[5]);
	bf_spec = mxGetPr(prhs[6]);

	plhs[0] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	plhs[1] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	plhs[2] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	numIt = mxGetPr(plhs[0]);
	optimal_rho = mxGetPr(plhs[1]);
	objVal = mxGetPr(plhs[2]);
    

    
	MKL_INT incx = 1;
	double sumd2 = cblas_dnrm2(NAnt, d, incx);
	sumd2 = sumd2 * sumd2;
	
    /*Main Routine*/
#pragma omp parallel
	{
		double bf_spec_perGrid;
		double *absz_perGrid, *swapPtr;
		double rho_left, rho_next = bf_spec[0];
		double *eig_left = (double*)mkl_malloc((NSrc - 1) * sizeof(double), 64); 
		double *tempDeriv = (double*)mkl_malloc(2 * sizeof(double), 64);
		MKL_INT it1, info;
		double optimal_rho_perGrid = -1.0;
		double *delta_mat = (double*)mkl_malloc((NAnt*(NSrc - 1)) * sizeof(double), 64);

		#pragma omp for
		for (iGrid = 0; iGrid < NGrid; iGrid++) {
			bf_spec_perGrid = bf_spec[iGrid];
			absz_perGrid = absz + iGrid*NAnt;
			it1 = 0;
			rho_next = rho_init[iGrid];
			while (true) {
				it1++;
				rho_left = rho_next;
				calcEig(d, rho_left, absz_perGrid, NAnt, NSrc, eig_left, delta_mat, info);
				
				calcDeriv(bf_spec_perGrid, rho_left, eig_left, absz_perGrid, delta_mat, NAnt, NSrc, tempDeriv);
				/* Newton's update*/
				rho_next = rho_left - tempDeriv[0] / tempDeriv[1];
				/* Special case when the next Newton's point of \rho is negative, then set the next point to some positive value*/
				if (rho_next < 0) {
					rho_next = 10 * rho_left;
				}
				/*Termination if the number of iterations exceeds some values or the objective functions does not change considerably*/
				if ((it1 > 100) || (fabs(rho_next - rho_left) < 1e-6) || (fabs(tempDeriv[0]) < 1e-6)) {
					break;
				}
			}
			optimal_rho[iGrid] = rho_next;
			/*Recalculate the required eigenvalues at the optimal \rho*/
			calcEig(d, optimal_rho[iGrid], absz_perGrid, NAnt, NSrc, eig_left, delta_mat, info);
			double sumEig2 = cblas_dnrm2(NSrc-1, eig_left, incx);
			sumEig2 = sumEig2*sumEig2;
			objVal[iGrid] -= sumEig2;
			objVal[iGrid] = objVal[iGrid] + sumd2 - 2 * bf_spec_perGrid*optimal_rho[iGrid] / NAnt + optimal_rho[iGrid] * optimal_rho[iGrid];
			numIt[iGrid] = it1;
		}
		/*Free the memory of the matrices*/
		mkl_free(delta_mat);
		mkl_free(eig_left);
		mkl_free(tempDeriv);
	}
}

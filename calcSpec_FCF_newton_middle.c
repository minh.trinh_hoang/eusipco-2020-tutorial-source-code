/* This function calculates the pseudo-spectrum of the PR-FCF estimator. The idea is
 * to perform Newton's method on the derivative of the objective function with respect
 * to \rho (or in the paper \sigma_s^2). The method to determine the eigenvalues is based 
 * on the paper "Solving Secular Function Stably and Efficiently" using the
 * Middle Way method. The difference between the PR-FCF and the PR-UCF estimator is
 * the consideration of noise power \sigma_n^2 in the optimization problem.
 * Input:
 *        d: a vector of size NAnt x 1: the initial eigenvalues without
 *           any perturbation, which is also the elements on the diagonal of
 *           the matrix D. Elements of d must be sorted descendingly
 *        absz2: a matrix of size NAnt x NGrid: contains the absolute
 *               value squared of the vector z=U^H*a for each grid point. 
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *		  bf_spec: a row vector of size 1 x NGrid: the beamformer spectrum
 *Output:
 *          objVal: a row vector of size 1 x NGrid: returns the optimal value of
 *					the PR-FCF optimization problem for each grid point
 *			sigma_s_opt: a row vector of size 1 x NGrid: returns the optimal value of
 *						 \sigma_s^2 for each grid point
 *			numIt: a row vector of size 1 x NGrid: returns the number of Newton steps
 *				   for each gridpoint
 */

#include "mex.h"
#include "matrix.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#ifndef mwSize
#define mwSize int
#endif

#ifndef mwIndex
#define mwIndex int
#endif

#ifndef mwPointer
#define mwPointer int
#endif

/* This subfunction calculates the first and second derivative of the objective function 
 * with respect to the scalar \rho (or equivalently \sigma_s^2). The objective function is rewritten to
 * utilize only (NSrc-1)- eigenvalues from calcEig function
 * Input:
 *        bf_spec_perGrid: the conventional beamformer spectrum at the given gridpoint
 *        sigma_s: the scalar sigma_s^2 at which the derivative is calculated
 *		  sumd: the scalar defined as the sum of all eigenvalues of the sample covariance matrix (or trace(R))
 *        eigVal: a row vector of size 1 x (NSrc-1): the eigenvalues of the modified Hermitian matrix
 *				  with the scalar \rho
 *		  absz2_perGrid: a row vector of size 1 x NAnt, containing the absolute valued squared of each entries in vector z in the paper
						 See Equation (65)
*		  d: a vector of size NAnt x 1, containing the initial eigenvalues
 *        NAnt: the size of the matrix D
 *        NSrc: the number of source signals (or the number of required eigenvalues plus one).
 *Output:
 *		  derivVal: the first and second derivative of the objective function at the point rho are saved
 *                  in the first and the second entry of derivVal
*/
void calcDeriv(double bf_spec_perGrid, double sigma_s, double sumd, double *eigVal, double *absz2_perGrid, double *d, mwSize NAnt, mwSize NSrc, double *derivVal) {
	mwSize iEig, iAnt;
	double sumEigDiff1, sumEig, sumEigDiff1_s, sumEigEigDiff1;
	double sumEigDiff2, sumEigEigDiff2;
	double derivSec1, derivSec1_perAnt;
	double derivSec2, derivSec2_perAnt;
	double eigdiff1, eigdiff2;
	double eigeigdiff1, eigeigdiff2;
	double sigma_sSquared;

	sumEigDiff1 = 0; sumEig = 0; sumEigDiff1_s = 0; sumEigEigDiff1 = 0;
	sumEigDiff2 = 0; sumEigEigDiff2 = 0;

	
	for (iEig = 0; iEig < (NSrc - 1); iEig++) {

		/*Calculate the first and the second derivative of the eigenvalues*/
		derivSec1 = 0;
		derivSec2 = 0;
		for (iAnt = 0; iAnt < NAnt; iAnt++) {
			derivSec1_perAnt = absz2_perGrid[iAnt] / ((d[iAnt] - eigVal[iEig]) * (d[iAnt] - eigVal[iEig]));
			derivSec2_perAnt = derivSec1_perAnt / (d[iAnt] - eigVal[iEig]);
			derivSec1 += derivSec1_perAnt;
			derivSec2 += derivSec2_perAnt;
		}
		sigma_sSquared = sigma_s*sigma_s;
		eigdiff1 = -1 / (sigma_sSquared*derivSec1);
		eigdiff2 = 2 / (sigma_sSquared*sigma_sSquared*derivSec1*derivSec1)*(sigma_s*derivSec1 - derivSec2 / derivSec1);

		/* Calculate the mixed terms*/
		eigeigdiff1 = eigVal[iEig] * eigdiff1;
		eigeigdiff2 = eigVal[iEig] * eigdiff2;

		/*Calculate the sums*/
		sumEigDiff1 += eigdiff1;
		sumEig += eigVal[iEig];
		sumEigDiff1_s +=  eigdiff1 *eigdiff1;
		sumEigEigDiff1 +=  eigeigdiff1;
		sumEigDiff2 += eigdiff2;
		sumEigEigDiff2 +=  eigeigdiff2;
	}

	derivVal[0] = -2*bf_spec_perGrid + 2*sigma_s*NAnt*NAnt - 2*sumEigEigDiff1 + 2*(sumd - sigma_s*NAnt - sumEig)*(sumEigDiff1 + NAnt) / (NAnt - NSrc + 1);
	derivVal[1] = 2 * NAnt*NAnt - 2*sumEigDiff1_s - 2*sumEigEigDiff2 + 2*((sumd - sigma_s*NAnt - sumEig)*sumEigDiff2 - (NAnt + sumEigDiff1)*(NAnt + sumEigDiff1)) / (NAnt - NSrc + 1);
}

/*This subfunction returns the (NSrc-1)-largest eigenvalues of the modified Hermitian matrix. 
* Unlike previous versions, this function can handle positive and negative value of sigma_s^2, 
* which can be used later for the Newton step. Furthermore, to be consistent with MATLAB,
* the eigenvalues are now sorted descendingly
* Input:
*        d: a vector of size NAnt x 1: the initial eigenvalues without
*           any perturbation, which is also the elements on the diagonal of
*           the matrix D. The elements of d must be sorted descendingly
*        rho: the scaling factor of the rank-one term. Must be scalar and positive
*        absz2_perGrid: a row vector of size 1 x NAnt: contains the absolute
*               value squared of the vector z for one grid point.
*        NAnt: the size of the matrix D
*        NSrc: the number of source signals (all the eigenvalues from the first to the (NSrc-1) will be calculated)
*
*Output:
*        eigVal: a vector of size (NSrc-1) x 1: returns the calculated
*                eigenvalues for each grid point.
*/
void calcEig(double *d, double rho, double *absz2_perGrid, mwSize NAnt, mwSize NSrc, double *eigVal) {
	mwSize iEig, iAnt;
	double Delta, f_mid, d_mid, d_NAnt1, h_dNAnt1;
	double psi, phi, deriv_psi, deriv_phi;
	double fy, deriv_fy, Delta_k, Delta_k1, y;
	double a,b,c, eta, pos_rho;
	double *obj_vec;
    double *diff_obj_vec;

	obj_vec = (double*)mxMalloc((NAnt) * sizeof(double));
	diff_obj_vec = (double*)mxMalloc(NAnt * sizeof(double));

	if (rho > 0) {
		for (iEig = 0; iEig < NSrc - 1; iEig++) {
			/*Initial guess*/
			Delta = -d[iEig + 1] + d[iEig];
			d_mid = -(d[iEig + 1] + d[iEig]) / 2;
			f_mid = 1 / rho; c = f_mid; /*c is also g(d_mid)*/
			for (iAnt = 0; iAnt < NAnt; iAnt++) {
				obj_vec[iAnt] = absz2_perGrid[iAnt] / (-d[iAnt] - d_mid);
				f_mid += obj_vec[iAnt];
				if ((iAnt != iEig) && (iAnt != (iEig + 1))) {
					c += obj_vec[iAnt];
				}
			}
			if (fabs(f_mid) < 1e-12) {
				y = d_mid;
			} else {
				if (f_mid > 0) {
					a = c*Delta + absz2_perGrid[iEig] + absz2_perGrid[iEig + 1];
					b = absz2_perGrid[iEig] * Delta;
					if (a <= 0) {
						y = -d[iEig] + (a - sqrt(a*a -4*b*c)) / (2 * c);
					}
					else {
						y = -d[iEig] + 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
				}
				else {
					a = -c*Delta + absz2_perGrid[iEig] + absz2_perGrid[iEig + 1];
					b = -absz2_perGrid[iEig + 1] * Delta;
					if (a <= 0) {
						y = -d[iEig + 1] + (a - sqrt(a*a - 4 * b*c)) / (2 * c);
					}
					else {
						y = -d[iEig + 1] + 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
				}
			} /*End of Initial guess*/
			/*MiddleWay Iteration*/
			while (true) {
				psi = 0; deriv_psi = 0;
				phi = 0; deriv_phi = 0;
				for (iAnt = 0; iAnt < NAnt; iAnt++) {
					obj_vec[iAnt] = absz2_perGrid[iAnt] / (-d[iAnt] - y);
					diff_obj_vec[iAnt] = obj_vec[iAnt]/(-d[iAnt] - y);
					if (iAnt <= iEig) {
						psi += obj_vec[iAnt];
						deriv_psi += diff_obj_vec[iAnt];
					}
					else {
						phi += obj_vec[iAnt];
						deriv_phi += diff_obj_vec[iAnt];
					}
				}
				fy = 1 / rho + phi + psi;
				if (fabs(fy) < 1e-9) {
					break;
				}
				deriv_fy = deriv_psi + deriv_phi;
				Delta_k = -d[iEig] - y;
				Delta_k1 = -d[iEig + 1] - y;

				a = (Delta_k + Delta_k1)*fy - Delta_k*Delta_k1*deriv_fy;
				b = Delta_k*Delta_k1*fy;
				c = fy - Delta_k*deriv_psi - Delta_k1*deriv_phi;

				if (a <= 0) {
					if (fabs(c) > 1e-12) {
						eta = (a - sqrt(a*a - 4 * b*c)) / (2 * c);
					} else {
						eta = 0;
					}
				}
				else { 
					if (fabs(a + sqrt(a*a - 4 * b*c)) > 1e-12) {
						eta = 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
					else {
						eta = 0;
					}
				}
				y = y + eta;
				if (fabs(eta) < 1e-12) {
					break;
				}
			}
			eigVal[iEig] = -y; /*End of MiddleWay Iteration*/
		}
	}
	else {
		pos_rho = -rho;
		/*Reverse the order. Here the special case: the largest eigenvalue*/
		/*Initial Guess*/
		d_NAnt1 = d[0];
		for (iAnt = 0; iAnt < NAnt; iAnt++) {
			d_NAnt1 = d_NAnt1 + pos_rho*absz2_perGrid[iAnt];
		}
		Delta = d[0] - d[1];
		d_mid = (d_NAnt1 + d[0]) / 2;
		c = 1/pos_rho;
		for (iAnt = 2; iAnt < NAnt; iAnt++) {
			obj_vec[iAnt] = absz2_perGrid[iAnt] / (d[iAnt] - d_mid);
			c += obj_vec[iAnt];
		}
		f_mid = c + absz2_perGrid[1]/(d[1] - d_mid) + absz2_perGrid[0] / (d[0] - d_mid);
		h_dNAnt1 = absz2_perGrid[1] / (d[1] - d_NAnt1) + absz2_perGrid[0] / (d[0] - d_NAnt1);
		if (fabs(f_mid) < 1e-12) {
			y = d_mid;
		}
		else {
			if (f_mid < 0) {
				if (c <= h_dNAnt1) {
					y = d_NAnt1;
				}
				else {
					a = -c*Delta + absz2_perGrid[1] + absz2_perGrid[0];
					b = -absz2_perGrid[0] * Delta;
					if (a >= 0) {
						y = d[0] + (a + sqrt(a*a - 4 * b*c)) / (2 * c);
					}
					else {
						y = d[0] + 2 * b / (a - sqrt(a*a - 4 * b*c));
					}
						
				}
			}
			else {
				a = -c*Delta + absz2_perGrid[1] + absz2_perGrid[0];
				b = -absz2_perGrid[0] * Delta;
				if (a >= 0) {
					y = d[0] + (a + sqrt(a*a - 4 * b*c)) / (2 * c);
				}
				else {
					y = d[0] + 2 * b / (a - sqrt(a*a - 4 * b*c));
				}
			}
		} /*End of Initial Guess*/
		/*MiddleWay Iteration for the largest eigenvalue*/
		while (true) {
			psi = 0; deriv_psi = 0;
			phi = 0; deriv_phi = 0;
			for (iAnt = 0; iAnt < NAnt; iAnt++) {
				obj_vec[iAnt] = absz2_perGrid[NAnt - iAnt - 1] / (d[NAnt - iAnt - 1] - y);
				diff_obj_vec[iAnt] = obj_vec[iAnt] / (d[NAnt - iAnt - 1] - y);
				if (iAnt <= NAnt - 2) {
					psi += obj_vec[iAnt];
					deriv_psi += diff_obj_vec[iAnt];
				}
				else {
					phi += obj_vec[iAnt];
					deriv_phi += diff_obj_vec[iAnt];
				}
			}
			fy = 1 / pos_rho + psi + phi;
			if (fabs(fy) < 1e-9) {
				break;
			}
			deriv_fy = deriv_psi + deriv_phi;
			Delta_k = d[1] - y;
			Delta_k1 = d[0] - y;

			a = (Delta_k + Delta_k1)*fy - Delta_k*Delta_k1*deriv_fy;
			b = Delta_k*Delta_k1*fy;
			c = fy - Delta_k*deriv_psi - Delta_k1*deriv_phi;
			if (a >= 0) {
				if (fabs(c) > 1e-12) {
					eta = (a + sqrt(a*a - 4 * b*c)) / (2 * c);
				}
				else {
					eta = 0;
				}
			}
			else {
				if (fabs(a - sqrt(a*a - 4 * b*c)) > 1e-12) {
					eta = 2 * b / (a - sqrt(a*a - 4 * b*c));
				}
				else {
					eta = 0;
				}
			}
			y = y + eta;
			if (fabs(eta) < 1e-12) {
				break;
			}
		}
		eigVal[0] = y; /*End of MiddleWay Iteration for the largest eigenvalue*/
		/*Calculation of the remaining eigenvalues*/
		for (iEig = 1; iEig < NSrc - 1; iEig++) {
			/*Initial Guess*/
			Delta = d[iEig - 1] - d[iEig];
			d_mid = (d[iEig - 1] + d[iEig]) / 2;
			f_mid = 1 / pos_rho; c = f_mid;
			for (iAnt = 0; iAnt < NAnt; iAnt++) {
				obj_vec[iAnt] = absz2_perGrid[iAnt] / (d[iAnt] - d_mid);
				f_mid = f_mid + obj_vec[iAnt];
				if ((iAnt != (iEig - 1)) && (iAnt != iEig)) {
					c = c + obj_vec[iAnt];
				}
			}
			if (fabs(f_mid) < 1e-12) {
				y = d_mid;
			}
			else {
				if (f_mid > 0) {
					a = c*Delta + absz2_perGrid[iEig] + absz2_perGrid[iEig - 1];
					b = absz2_perGrid[iEig] * Delta;
					if (a <= 0) {
						y = d[iEig] + (a - sqrt(a*a - 4 * b*c)) / (2 * c);
					}
					else {
						y = d[iEig] + 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
				}
				else {
					a = -c*Delta + absz2_perGrid[iEig] + absz2_perGrid[iEig - 1];
					b = -absz2_perGrid[iEig - 1] * Delta;
					if (a <= 0) {
						y = d[iEig - 1] + (a - sqrt(a*a - 4 * b*c)) / (2 * c);
					}
					else {
						y = d[iEig - 1] + 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
				}
			} /*End of initial guess*/
			/*MiddleWay Iteration*/
			while (true) {
				psi = 0; deriv_psi = 0;
				phi = 0; deriv_phi = 0;
				for (iAnt = NAnt-1; iAnt >= 0; iAnt--) {
					obj_vec[iAnt] = absz2_perGrid[iAnt] / (d[iAnt] - y);
					diff_obj_vec[iAnt] = obj_vec[iAnt] / (d[iAnt] - y);
					if (iAnt >= iEig) {
						psi += obj_vec[iAnt];
						deriv_psi += diff_obj_vec[iAnt];
					}
					else {
						phi += obj_vec[iAnt];
						deriv_phi += diff_obj_vec[iAnt];
					}
				}
				fy = 1 / pos_rho + phi + psi;
				if (fabs(fy) < 1e-9) {
					break;
				}
				deriv_fy = deriv_psi + deriv_phi;
				Delta_k = d[iEig] - y;
				Delta_k1 = d[iEig - 1] - y;

				a = (Delta_k + Delta_k1)*fy - Delta_k*Delta_k1*deriv_fy;
				b = Delta_k*Delta_k1*fy;
				c = fy - Delta_k*deriv_psi - Delta_k1*deriv_phi;

				if (a <= 0) {
					if (fabs(c) > 1e-12) {
						eta = (a - sqrt(a*a - 4 * b*c)) / (2 * c);
					}
					else {
						eta = 0;
					}
				}
				else {
					if (fabs(a + sqrt(a*a - 4 * b*c)) > 1e-12) {
						eta = 2 * b / (a + sqrt(a*a - 4 * b*c));
					}
					else {
						eta = 0;
					}
				}
				y = y + eta;
				if (fabs(eta) < 1e-12) {
					break;
				}

			}

			eigVal[iEig] = y;
		}
	}
	mxFree(obj_vec);
	mxFree(diff_obj_vec);
}


void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[])
{
    /* input variables*/
    double *d;
    double *absz2;
    mwSize NSrc;
	double *bf_spec;
	/* output matrix */
    double *numIt;             
	double *sigma_s_opt;
	double *objVal;

    /*temporary variables in the routine*/
	mwSize NAnt, NGrid;
	mwSize iGrid, iAnt, iEig;
	double *absz2_perGrid;
	mwSize it1;
	double sumd, sumd2, bf_spec_perGrid, zero_threshold, sum_noiseEig_s, sum_noiseEig;

	double sigma_sLeft, sigma_sNext;
	double *eig_left, *temporaryDeriv;

	double sumEigVal_s, sumEigVal;
    /* Initialize values for input variables*/
    d = mxGetPr(prhs[0]);
    absz2 = mxGetPr(prhs[1]);
    NSrc = mxGetScalar(prhs[2]);
	bf_spec = mxGetPr(prhs[3]);

	/* Predefine some values*/
	NAnt = (mwSize)(mxGetM(prhs[1]));
	NGrid = (mwSize)(mxGetN(prhs[1]));

	/* Initialize values for output variables*/

    plhs[0] = mxCreateDoubleMatrix(1, NGrid,mxREAL);
	plhs[1] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	plhs[2] = mxCreateDoubleMatrix(1, NGrid, mxREAL);
	objVal = mxGetPr(plhs[0]);
	sigma_s_opt = mxGetPr(plhs[1]);
	numIt = mxGetPr(plhs[2]);
    
	
    /* Preallocate memory for the matrices*/
	eig_left = (double*)mxMalloc(((mwSize)(NSrc - 1)) * sizeof(double));
	temporaryDeriv = (double*)mxMalloc(2 * sizeof(double));
	absz2_perGrid = (double*)mxMalloc(((mwSize)NAnt) * sizeof(double));
	
    /* Calculate the sum of the initial eigenvalues squared (corresponding to tr(R^2))*/
	sumd2 = 0;
	sumd = 0;
	sum_noiseEig = 0; sum_noiseEig_s = 0;
	for (iAnt = 0; iAnt < NAnt; iAnt++) {
		sumd2 = sumd2 + d[iAnt]*d[iAnt];
		sumd = sumd + d[iAnt];
		if (iAnt >= NSrc - 1) {
			sum_noiseEig += d[iAnt];
			sum_noiseEig_s += d[iAnt] * d[iAnt];
		}
	}
	zero_threshold = sum_noiseEig_s - sum_noiseEig*sum_noiseEig / (NAnt - NSrc + 1);
    
    
    
    /*Main Routine*/
	for (iGrid = 0; iGrid < NGrid; iGrid++) {
		sigma_sNext = bf_spec[iGrid];
		absz2_perGrid = &absz2[iGrid*NAnt];
		bf_spec_perGrid = bf_spec[iGrid];
		it1 = 0;
		//if (iGrid == 48) {
		//	break;
		//}

		while (true) {
			it1++;
			sigma_sLeft = sigma_sNext;
            /*Calculate the first and second derivative at the current point \rho_left*/
			calcEig(d, sigma_sLeft, absz2_perGrid, NAnt, NSrc, eig_left);
			calcDeriv(bf_spec_perGrid, sigma_sLeft, sumd, eig_left, absz2_perGrid, d, (mwSize)NAnt, (mwSize)NSrc, temporaryDeriv);
            /* Newton's update*/
			sigma_sNext = sigma_sLeft - temporaryDeriv[0] / temporaryDeriv[1];
			
            /*Termination if the number of iterations exceeds some values or the objective functions does not change considerably*/
			if ((it1 > 50) || (fabs(sigma_sNext - sigma_sLeft) < 1e-6) || (fabs(temporaryDeriv[0])< 1e-6)) {
				break;
			}
		}
		sigma_s_opt[iGrid] = sigma_sNext;
        /*Recalculate the required eigenvalues at the optimal \rho*/
		calcEig(d, sigma_s_opt[iGrid], absz2_perGrid, (mwSize)NAnt, (mwSize)NSrc, eig_left);
        /*Assign the optimal value of the objective function and the total number of iterations where the eigenvalues computation is required*/
		sumEigVal = 0; sumEigVal_s = 0;
		for (iEig = 0; iEig < (NSrc - 1); iEig++) {
			sumEigVal += eig_left[iEig];
			sumEigVal_s += eig_left[iEig] * eig_left[iEig];
		}
		objVal[iGrid] = sumd2 - 2*sigma_s_opt[iGrid]*bf_spec_perGrid + sigma_s_opt[iGrid]* sigma_s_opt[iGrid]*NAnt*NAnt - sumEigVal_s - (sumd - sigma_s_opt[iGrid]*NAnt-sumEigVal)*(sumd - sigma_s_opt[iGrid] * NAnt - sumEigVal)/ (NAnt - NSrc + 1);
		numIt[iGrid] = it1;
		if ((objVal[iGrid] > zero_threshold) || (sigma_s_opt[iGrid] < 0)) {
			objVal[iGrid] = zero_threshold;
			sigma_s_opt[iGrid] = 0;
		}
	}
}

clc;
clear variables;
close all;
rng('default');
addpath('MLEstimators')
%% Define simulation setup 
% number of antennas
NAnt = 5;
% Direction of Arrivals in degrees (from -90 to 90 degrees)
DOA = [0;5];
% Number of sources
NSrc = length(DOA);
% ULA steering matrix
aDeg = @(DOA)exp(1j*pi*(0:NAnt-1).'*sin(DOA(:).'/180*pi));
A = aDeg(DOA);
% Number of snapshots
NSnp = 50;
% SNR at the receiver in dB
SNRdB = 10;
%% Signal Model
% Transmitted signal
S = 1/sqrt(2)*(randn(NSrc, NSnp) + 1j*randn(NSrc, NSnp));
corrFactor = 0;
Rss = eye(NSrc);
Rss(1,2) = corrFactor; Rss(2,1) = corrFactor';
% Noise at the receiver
N = 10^(-SNRdB/20)/sqrt(2)*(randn(NAnt, NSnp) + 1j*randn(NAnt, NSnp));
% Received signal
Y = A*sqrtm(Rss)*S + N;
% Grid points of the spectrum and the corresponding steering dictionary
NGrid = 1800;
Grid = linspace(-90, 90, NGrid);
AGrid = aDeg(Grid);
%% Estimate the DOAs using PR methods
% Define the function handle to call the wanted function
DOAfunc = @prwsf;
% Compute the estimated DOAs and the corresponding spectrum
tic
[estDOA, estSpec] = DOAfunc(Y, NSrc, Grid, AGrid);
toc
% Results
estDOA

figure
plot(Grid, estSpec);
xlim([-90, 90]);
xlabel('DOA (degrees)');
ylabel('Normalized Pseudo-spectrum');
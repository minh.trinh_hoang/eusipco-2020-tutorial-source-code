function [ DOA_est, Spec_est, sigma_s_opt, numIt] = prfcf_newton( Y, NSrc, Grid, AGrid, varargin )
%PRFCF_NEWTON returns the estimated DOA from the received signal and the
%estimated spectrum of the full covariance fitting where the estimated power
%from each direction is determined by Newton's method. 
%
%Input:   Y: Received signal (NAnt x NSnp)
%         NSrc: Number of sources
%         Grid: the sampled angle of view (1 x NGrid)
%         AGrid: the dictionary steering matrix corresponding to the
%         sampled angle of view (NAnt x NGrid)
%         Ryy: the covariance matrix of the received signal (optional)
%
% Output: DOA_est: the estimated DOA on the sampled angle of view (1 x NSrc)
%         Spec_est: the normalized pseudo-spectrum on the grid,  (1 x NGrid)
%                   where the highest peak has the unit height


% Initialize parameters
[~, NSnp] = size(Y);
if isempty(varargin)
    R_est = 1/NSnp*(Y*Y');
else
    R_est = varargin{1};
end

% Perform full eigenvalue decomposition and diagonalization
[U, Lambda_hat] = eig(R_est);
d = diag(real(Lambda_hat));
[d, order] = sort(d, 'descend');
U = U(:, order);

% Calculate the absolute value squared of each entry in the z vector for
% each angle of view
ATilde = U'*AGrid;
absz2 = abs(ATilde).^2;
bf_spec = sum(bsxfun(@times, absz2, d), 1);

% Computation of the spectrum
[objVal, sigma_s_opt, numIt] = calcSpec_FCF_newton_middle(d, absz2, NSrc, bf_spec);
Spec_est = 1./(objVal+1e-3);
Spec_est = Spec_est./max(Spec_est);

% Estimate the DOA based on the pseudo-spectrum
[~, locs] = findpeaks(Spec_est, 'SortStr', 'descend');
if length(locs)> NSrc
    DOA_est = Grid(locs(1:NSrc));
else
    DOA_est = [Grid(locs), 90*ones(1, NSrc - length(locs))];
end
DOA_est = sort(DOA_est(:));

end
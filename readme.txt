An example of calling PR methods, which were implemented in *.c, can be 
found in the main script "main.m". The PR methods use BLAS and LAPACK
functions from the Intel MKL and OMP for parallelization. For more
information, please follow the instruction from the Intel Development
Website on how to link the code with the Intel MKL Library.

The available code can be readily executed in Linux and Windows.

If you find any errors or bugs in the source code, please contact me at
thminh@nt.tu-darmstadt.de